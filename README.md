# Python Simple Exercises


## Description
Those are some exercises on python using tools and techniques that are useful for a data engineer. 

## License
Those exercises are only for demo purposes. They are not for commercial use. You can download and solve them for your own practice.


Thank you! 
